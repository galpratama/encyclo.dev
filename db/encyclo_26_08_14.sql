-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2014 at 10:46 PM
-- Server version: 5.6.16
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `encyclo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ncy_admin`
--

CREATE TABLE IF NOT EXISTS `ncy_admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `name_first` varchar(23) NOT NULL,
  `name_last` varchar(32) NOT NULL,
  `access_rights` varchar(11) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_banners`
--

CREATE TABLE IF NOT EXISTS `ncy_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `enable_time` date NOT NULL,
  `disable_time` date NOT NULL,
  `image` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ncy_banners`
--

INSERT INTO `ncy_banners` (`id`, `title`, `enable_time`, `disable_time`, `image`, `link`, `new_window`, `position`) VALUES
(9, 'Boomphones Sale', '2014-08-25', '2014-08-30', '20140825073759-Boomphones-Sale.jpg', 'http://encyclo.dev/', 0, 0),
(10, 'Cooko 2', '2014-08-25', '2014-08-28', '20140825073819-Cooko-2.jpg', 'http://encyclo.dev/', 0, 0),
(11, 'Gamers Never Stop', '2014-08-25', '2014-08-28', '20140825073853-Gamers-Never-Stop.jpg', 'http://encyclo.dev/', 0, 0),
(12, 'Paket Narsis', '2014-08-27', '2014-08-31', '20140825074017-Paket-Narsis.jpg', 'http://encyclo.dev/', 0, 0),
(13, 'Energy Power Meter', '2014-08-29', '2014-08-31', '20140825074039-Energy-Power-Meter.jpg', 'http://encyclo.dev/', 0, 0),
(14, 'Keychain Power Bank', '2014-09-05', '2014-09-11', '20140825074132-Keychain-Power-Bank.jpg', 'http://encyclo.dev/', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ncy_changelog`
--

CREATE TABLE IF NOT EXISTS `ncy_changelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `changelog_type` varchar(128) NOT NULL,
  `changelog_text` varchar(512) NOT NULL,
  `changelog_link` varchar(384) DEFAULT NULL,
  `changelog_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ncy_changelog`
--

INSERT INTO `ncy_changelog` (`id`, `changelog_type`, `changelog_text`, `changelog_link`, `changelog_date`) VALUES
(1, 'insert', 'Pages <strong>dsa</strong> Successfully Added', NULL, '2014-08-25 20:25:08'),
(3, 'insert', 'Pages <strong>Tesadd</strong> Successfully Added', NULL, '2014-08-25 20:32:50'),
(4, 'update', 'Pages <strong>Random</strong> Successfully Edited', NULL, '2014-08-25 20:33:09'),
(5, 'delete', 'Pages/Link <strong>Random</strong> Successfully Deleted', NULL, '2014-08-25 20:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `ncy_coupons`
--

CREATE TABLE IF NOT EXISTS `ncy_coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `whole_order_coupon` tinyint(1) NOT NULL,
  `max_product_instances` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `max_uses` mediumint(8) unsigned NOT NULL,
  `num_uses` mediumint(8) unsigned NOT NULL,
  `reduction_target` varchar(8) NOT NULL,
  `reduction_amount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_featured`
--

CREATE TABLE IF NOT EXISTS `ncy_featured` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `enable_time` date NOT NULL,
  `disable_time` date NOT NULL,
  `image` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `position` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_guest_customer`
--

CREATE TABLE IF NOT EXISTS `ncy_guest_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `email_subscribe` tinyint(1) DEFAULT NULL,
  `phone` varchar(32) NOT NULL,
  `company` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_orders`
--

CREATE TABLE IF NOT EXISTS `ncy_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(60) NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `ordered_on` datetime NOT NULL,
  `shipped_on` datetime DEFAULT NULL,
  `tax` float(10,2) NOT NULL,
  `total` float(10,2) NOT NULL,
  `subtotal` float(10,2) NOT NULL,
  `coupon_discount` float(10,2) NOT NULL,
  `shipping` float(10,2) NOT NULL,
  `shipping_notes` text NOT NULL,
  `shipping_method` tinytext NOT NULL,
  `notes` text,
  `payment_info` text NOT NULL,
  `referral` text NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `phone` varchar(40) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ship_company` varchar(255) DEFAULT NULL,
  `ship_firstname` varchar(255) DEFAULT NULL,
  `ship_lastname` varchar(255) DEFAULT NULL,
  `ship_email` varchar(255) DEFAULT NULL,
  `ship_phone` varchar(40) DEFAULT NULL,
  `ship_address1` varchar(255) DEFAULT NULL,
  `ship_address2` varchar(255) DEFAULT NULL,
  `ship_city` varchar(255) DEFAULT NULL,
  `ship_zip` varchar(11) DEFAULT NULL,
  `ship_zone` varchar(255) DEFAULT NULL,
  `ship_zone_id` int(11) DEFAULT NULL,
  `ship_country` varchar(255) DEFAULT NULL,
  `ship_country_code` varchar(10) DEFAULT NULL,
  `ship_country_id` int(11) DEFAULT NULL,
  `bill_company` varchar(255) DEFAULT NULL,
  `bill_firstname` varchar(255) DEFAULT NULL,
  `bill_lastname` varchar(255) DEFAULT NULL,
  `bill_email` varchar(255) DEFAULT NULL,
  `bill_phone` varchar(40) DEFAULT NULL,
  `bill_address1` varchar(255) DEFAULT NULL,
  `bill_address2` varchar(255) DEFAULT NULL,
  `bill_city` varchar(255) DEFAULT NULL,
  `bill_zip` varchar(11) DEFAULT NULL,
  `bill_zone` varchar(255) DEFAULT NULL,
  `bill_zone_id` int(11) DEFAULT NULL,
  `bill_country` varchar(255) DEFAULT NULL,
  `bill_country_code` varchar(10) DEFAULT NULL,
  `bill_country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_orders_item`
--

CREATE TABLE IF NOT EXISTS `ncy_orders_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncy_pages`
--

CREATE TABLE IF NOT EXISTS `ncy_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `title_link` varchar(255) NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `seo_title` text NOT NULL,
  `seo_meta` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '0 (deleted) 1 (published), 2 (inactive)',
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `slug_2` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `ncy_pages`
--

INSERT INTO `ncy_pages` (`id`, `parent_id`, `title`, `title_link`, `url`, `slug`, `content`, `seo_title`, `seo_meta`, `date`, `status`, `new_window`) VALUES
(5, 0, 'FAQ', 'FAQ', NULL, 'faq', '<p>Sneaker bomber chignon lilac floral ecru clutch cami seam bandeau. Shoe Maison Martin Margiela collarless center part Jil Sander Vasari crop la marini&egrave;re. Sports luxe neutral razor pleats Cara D. chambray vintage braid dove grey minimal. Pastel white slipper Lanvin So-Cal playsuit denim shorts.</p>\r\n<p>Grunge powder blue skinny jeans spearmint vintage Levi silhouette. Chunky sole leggings tortoise-shell sunglasses holographic oxford denim dress metallic black Prada Saffiano. A.P.C. Acne tucked t-shirt cotton C&eacute;line. Tee slip dress 90s washed out strong eyebrows cable knit.</p>\r\n<p>Dungaree print luxe midi skort cuff plaited flats navy blue ribbed. Button up Paris rings statement texture oversized sweatshirt Saint Laurent boots. Knot ponytail indigo skirt skirt loafer cashmere sandal. Furla Givenchy knitwear round sunglasses Herm&egrave;s.</p>\r\n<p><img src="../../uploads/tinymce/58c07f1165b7796571218e4228f9e15c.jpg" alt="" /></p>\r\n<p>Relaxed leather tote envelope clutch maxi leather. Black skirt neutral holographic denim shorts statement minimal skirt vintage chambray. Leather tote C&eacute;line bomber flats razor pleats white shirt. Center part chignon cami white shoe envelope clutch.</p>\r\n<p>Rings Paris knot ponytail street style collarless seam slipper powder blue. Washed out vintage Levi knitwear midi Acne. Spearmint Cara D. plaited ecru bandeau. Oxford dress clutch navy blue Maison Martin Margiela luxe.</p>', 'FAQ', '', '2014-08-25 06:49:36', 1, 0),
(6, 0, 'Cara Pembelian', 'Cara Pembelian', NULL, 'cara-pembelian', '<p>Pastel ecru tee maxi So-Cal. Shoe collarless strong eyebrows crop bomber la marini&egrave;re cami. Givenchy washed out round sunglasses Cara D. texture midi. Black powder blue white shirt leather indigo Furla Maison Martin Margiela chunky sole bandeau.</p>\n<p>Knot ponytail lilac seam slip dress silhouette print cuff navy blue button up. Ribbed denim shorts tucked t-shirt metallic denim. Skort dress Jil Sander Vasari leggings cable knit. Luxe vintage Levi leather tote grunge rings statement Lanvin.</p>\n<p><img src="../../uploads/tinymce/1f4680bbb724e7790a97a9922c168464.jpg" alt="" /></p>\n<p>Relaxed neutral flats center part dove grey. Braid playsuit 90s sports luxe skinny jeans cashmere floral knitwear minimal skirt. White Paris Herm&egrave;s sneaker razor pleats spearmint. Dungaree Prada Saffiano gold collar clutch envelope clutch oversized sweatshirt plaited C&eacute;line sandal.</p>\n<p>Chambray chignon Acne cotton oxford boots vintage holographic street style slipper. Vintage oversized sweatshirt seam razor pleats luxe knitwear. Braid spearmint lilac Cara D. print dove grey. Chignon skirt round sunglasses oxford skort ecru.</p>\n<p>Slip dress rings leggings C&eacute;line Herm&egrave;s skinny jeans minimal leather Paris. Plaited leather tote gold collar cuff bomber denim shorts tortoise-shell sunglasses neutral Prada Saffiano tucked t-shirt. Pastel boots washed out button up chunky sole. Playsuit statement dress envelope clutch shoe.</p>', 'Bagaimana Cara Memesan Barang di Shafira Encyclo?', '', '2014-08-25 06:50:54', 1, 0),
(7, 0, 'Konfirmasi Pembayaran', 'Konfirmasi Pembayaran', NULL, 'konfirmasi-pembayaran', '<p>Holographic ribbed skort metallic playsuit chignon Furla. Ecru button up black la marini&egrave;re oxford slip dress grunge plaited washed out. Skinny jeans loafer Jil Sander Vasari flats rings cotton midi print. Acne round sunglasses collarless cuff center part relaxed shoe silhouette Maison Martin Margiela.</p>\r\n<p>Street style floral Givenchy boots tee powder blue knot ponytail. Navy blue texture dress C&eacute;line pastel cami denim shorts slipper braid. Neutral knitwear vintage Levi skirt white. Leggings maxi spearmint dungaree Prada Saffiano cable knit sports luxe bomber.</p>\r\n<p>White shirt envelope clutch razor pleats lilac Cara D. Lanvin leather Herm&egrave;s crop A.P.C. denim chambray minimal. Luxe cashmere statement 90s bandeau gold collar. So-Cal clutch indigo oversized sweatshirt chunky sole dove grey tortoise-shell sunglasses sandal sneaker.</p>\r\n<p>Relaxed Cara D. boots cashmere razor pleats chignon envelope clutch round sunglasses. Givenchy la marini&egrave;re 90s white shirt cuff chunky sole vintage Levi knot ponytail. Grunge skirt print gold collar So-Cal Maison Martin Margiela Prada Saffiano metallic. Dove grey tee rings denim Furla neutral.</p>\r\n<p>Flats center part tortoise-shell sunglasses braid spearmint. Saint Laurent dress shoe maxi navy blue skinny jeans cable knit skort statement cami. Oxford midi floral clutch loafer white bomber ecru chambray pastel. Crop denim shorts slip dress Paris plaited.</p>', 'Cara Konfirmasi Pembayaran', '', '2014-08-25 06:53:02', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
