--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.13
-- Dumped by pg_dump version 9.1.13
-- Started on 2014-08-26 10:09:54 WIB

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 165 (class 3079 OID 11645)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1888 (class 0 OID 0)
-- Dependencies: 165
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 177 (class 1255 OID 27426)
-- Dependencies: 504 6
-- Name: get_id_detail_t(character varying); Type: FUNCTION; Schema: public; Owner: erp
--

CREATE FUNCTION get_id_detail_t(ptable character varying) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	return_v NUMERIC;
BEGIN
	return_v=0;

	IF ptable='t_pencairan_poin_detail' THEN
		SELECT COALESCE(MAX(t_pencairan_poin_detail_id),0)+1 INTO return_v FROM t_pencairan_poin_detail;
	END IF;

	RETURN return_v;
	
END;
$$;


ALTER FUNCTION public.get_id_detail_t(ptable character varying) OWNER TO erp;

--
-- TOC entry 178 (class 1255 OID 27427)
-- Dependencies: 504 6
-- Name: set_poin_t(numeric, numeric); Type: FUNCTION; Schema: public; Owner: erp
--

CREATE FUNCTION set_poin_t(id_bp_p numeric, poin_cair_p numeric) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	--return_v NUMERIC;
	myrow t_akumulasi_poin%ROWTYPE;
BEGIN
	--return_v=NULL;
	
	SELECT * INTO myrow FROM t_akumulasi_poin WHERE r_bp_id=id_bp_p;
	IF myrow.totalpoin = myrow.totalpoin_reserve THEN
		UPDATE t_akumulasi_poin SET totalpoin=totalpoin-poin_cair_p,totalpoin_reserve=totalpoin_reserve-poin_cair_p WHERE r_bp_id=id_bp_p;
	ELSE
		UPDATE t_akumulasi_poin SET totalpoin=totalpoin-poin_cair_p WHERE r_bp_id=id_bp_p;
	END IF;

	RETURN NULL;
	
END;
$$;


ALTER FUNCTION public.set_poin_t(id_bp_p numeric, poin_cair_p numeric) OWNER TO erp;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 27343)
-- Dependencies: 6
-- Name: m_poin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE m_poin (
    nilai_poin_belanja numeric,
    nilai_poin_hadiah numeric,
    grup_org numeric(2,0) NOT NULL
);


ALTER TABLE public.m_poin OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 27349)
-- Dependencies: 6
-- Name: t_akumulasi_poin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE t_akumulasi_poin (
    r_bp_id numeric NOT NULL,
    nomember character varying(25),
    nama character varying(150),
    nohp character varying(50),
    email character varying(50),
    totalpoin numeric,
    totalpoin_reserve numeric
);


ALTER TABLE public.t_akumulasi_poin OWNER TO postgres;

--
-- TOC entry 163 (class 1259 OID 27355)
-- Dependencies: 6
-- Name: t_pencairan_poin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE t_pencairan_poin (
    notrans character varying(20) NOT NULL,
    tanggal date,
    r_bp_id numeric,
    keterangan character varying(150),
    tempat_pencairan character varying(100),
    total_poin numeric,
    kdstatusdokumen character varying(2)
);


ALTER TABLE public.t_pencairan_poin OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 27361)
-- Dependencies: 6
-- Name: t_pencairan_poin_detail; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE t_pencairan_poin_detail (
    t_pencairan_poin_detail_id numeric NOT NULL,
    notrans character varying(20) NOT NULL,
    kode_produk character varying(20),
    r_produk_id numeric,
    qty numeric,
    jmlpoin numeric,
    nilaipoin numeric
);


ALTER TABLE public.t_pencairan_poin_detail OWNER TO postgres;

--
-- TOC entry 1877 (class 0 OID 27343)
-- Dependencies: 161 1881
-- Data for Name: m_poin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY m_poin (nilai_poin_belanja, nilai_poin_hadiah, grup_org) FROM stdin;
100000	10000	14
150000	10000	1
\.


--
-- TOC entry 1878 (class 0 OID 27349)
-- Dependencies: 162 1881
-- Data for Name: t_akumulasi_poin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_akumulasi_poin (r_bp_id, nomember, nama, nohp, email, totalpoin, totalpoin_reserve) FROM stdin;
10000152	01.10.1111111	Hilman Firmansyah	08997883228	\N	51	51
10000162	123321123321	Tatang T	085	kang@gmail.com	118	118
10000170	999999	Doni	0390230200	edi.junaedi@shafco.com	5	5
10000153	01884948	Edi Junaedi	08172315418	\N	2	2
\.


--
-- TOC entry 1879 (class 0 OID 27355)
-- Dependencies: 163 1881
-- Data for Name: t_pencairan_poin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pencairan_poin (notrans, tanggal, r_bp_id, keterangan, tempat_pencairan, total_poin, kdstatusdokumen) FROM stdin;
XXX.20082014.001CP	2014-08-20	10000162	\N	XXX	24	\N
101.22082014.001CP	2014-08-22	10000162	Test Pencairan 22 -08	101	2	\N
101.22082014.002CP	2014-08-22	10000170	\N	101	2	\N
101.23082014.001CP	2014-08-23	10000170	\N	101	2	\N
XXX.19082014.001CP	2014-08-19	10000162	\N	XXX	55	\N
\.


--
-- TOC entry 1880 (class 0 OID 27361)
-- Dependencies: 164 1881
-- Data for Name: t_pencairan_poin_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY t_pencairan_poin_detail (t_pencairan_poin_detail_id, notrans, kode_produk, r_produk_id, qty, jmlpoin, nilaipoin) FROM stdin;
1	XXX.19082014.001CP	ZWK0.J1.4001	1100448	2	\N	470000
2	XXX.19082014.001CP	S001.41.0001	1095459	3	\N	72000
3	XXX.20082014.001CP	ZWK0.J1.4001	1100448	1	\N	235000
4	101.22082014.001CP	S004.08.0004	1095735	1	\N	12000
5	101.22082014.002CP	S004.08.0004	1095735	1	\N	12000
6	101.23082014.001CP	S004.08.0004	1095735	1	\N	12000
\.


--
-- TOC entry 1768 (class 2606 OID 27376)
-- Dependencies: 161 161 1882
-- Name: m_poin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY m_poin
    ADD CONSTRAINT m_poin_pkey PRIMARY KEY (grup_org);


--
-- TOC entry 1770 (class 2606 OID 27370)
-- Dependencies: 162 162 1882
-- Name: t_akumulasi_poin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_akumulasi_poin
    ADD CONSTRAINT t_akumulasi_poin_pkey PRIMARY KEY (r_bp_id);


--
-- TOC entry 1774 (class 2606 OID 27372)
-- Dependencies: 164 164 164 1882
-- Name: t_pencairan_poin_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_pencairan_poin_detail
    ADD CONSTRAINT t_pencairan_poin_detail_pkey PRIMARY KEY (t_pencairan_poin_detail_id, notrans);


--
-- TOC entry 1772 (class 2606 OID 27374)
-- Dependencies: 163 163 1882
-- Name: t_pencairan_poin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY t_pencairan_poin
    ADD CONSTRAINT t_pencairan_poin_pkey PRIMARY KEY (notrans);


--
-- TOC entry 1775 (class 2606 OID 27441)
-- Dependencies: 1771 163 164 1882
-- Name: fk_header; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY t_pencairan_poin_detail
    ADD CONSTRAINT fk_header FOREIGN KEY (notrans) REFERENCES t_pencairan_poin(notrans) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1887 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 1889 (class 0 OID 0)
-- Dependencies: 161
-- Name: m_poin; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE m_poin FROM PUBLIC;
REVOKE ALL ON TABLE m_poin FROM postgres;
GRANT ALL ON TABLE m_poin TO postgres;
GRANT SELECT,UPDATE ON TABLE m_poin TO other;


--
-- TOC entry 1890 (class 0 OID 0)
-- Dependencies: 162
-- Name: t_akumulasi_poin; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE t_akumulasi_poin FROM PUBLIC;
REVOKE ALL ON TABLE t_akumulasi_poin FROM postgres;
GRANT ALL ON TABLE t_akumulasi_poin TO postgres;
GRANT SELECT,UPDATE ON TABLE t_akumulasi_poin TO other;


--
-- TOC entry 1891 (class 0 OID 0)
-- Dependencies: 163
-- Name: t_pencairan_poin; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE t_pencairan_poin FROM PUBLIC;
REVOKE ALL ON TABLE t_pencairan_poin FROM postgres;
GRANT ALL ON TABLE t_pencairan_poin TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE t_pencairan_poin TO other;


--
-- TOC entry 1892 (class 0 OID 0)
-- Dependencies: 164
-- Name: t_pencairan_poin_detail; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE t_pencairan_poin_detail FROM PUBLIC;
REVOKE ALL ON TABLE t_pencairan_poin_detail FROM postgres;
GRANT ALL ON TABLE t_pencairan_poin_detail TO postgres;
GRANT SELECT,INSERT,UPDATE ON TABLE t_pencairan_poin_detail TO other;


-- Completed on 2014-08-26 10:09:57 WIB

--
-- PostgreSQL database dump complete
--

