<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

/* Webstore Database */

$active_group = 'webstore';
$active_record = TRUE;

$db['webstore']['hostname'] = 'localhost';
$db['webstore']['username'] = 'root';
$db['webstore']['password'] = 'mysql';
$db['webstore']['database'] = 'encyclo';
$db['webstore']['dbdriver'] = 'mysql';
$db['webstore']['dbprefix'] = 'ncy_';
$db['webstore']['pconnect'] = TRUE;
$db['webstore']['db_debug'] = TRUE;
$db['webstore']['cache_on'] = FALSE;
$db['webstore']['cachedir'] = '';
$db['webstore']['char_set'] = 'utf8';
$db['webstore']['dbcollat'] = 'utf8_general_ci';
$db['webstore']['swap_pre'] = '';
$db['webstore']['autoinit'] = TRUE;
$db['webstore']['stricton'] = FALSE;

/* POS Database */


$active_group = 'pos';
$active_record = TRUE;

$db['pos']['hostname'] = 'localhost';
$db['pos']['username'] = 'postgres';
$db['pos']['password'] = 'postgres';
$db['pos']['database'] = 'shafco_erp_trial_';
$db['pos']['dbdriver'] = 'postgre';
$db['pos']['dbprefix'] = '';
$db['pos']['pconnect'] = TRUE;
$db['pos']['db_debug'] = TRUE;
$db['pos']['cache_on'] = FALSE;
$db['pos']['cachedir'] = '';
$db['pos']['char_set'] = 'utf8';
$db['pos']['dbcollat'] = 'utf8_general_ci';
$db['pos']['swap_pre'] = '';
$db['pos']['autoinit'] = TRUE;
$db['pos']['stricton'] = FALSE;

/* Poin Database */

$active_group = 'poin';
$active_record = TRUE;

$db['poin']['hostname'] = 'localhost';
$db['poin']['username'] = 'postgres';
$db['poin']['password'] = 'postgres';
$db['poin']['database'] = 'shafco_poinbelanja';
$db['poin']['dbdriver'] = 'postgre';
$db['poin']['dbprefix'] = '';
$db['poin']['pconnect'] = TRUE;
$db['poin']['db_debug'] = TRUE;
$db['poin']['cache_on'] = FALSE;
$db['poin']['cachedir'] = '';
$db['poin']['char_set'] = 'utf8';
$db['poin']['dbcollat'] = 'utf8_general_ci';
$db['poin']['swap_pre'] = '';
$db['poin']['autoinit'] = TRUE;
$db['poin']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */
