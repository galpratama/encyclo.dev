<?php if (!defined('BASEPATH')) die();
class Cart extends Main_Controller {

    public function index()
    {
        $data['view']  = 'cart';

        $this->load->view('template', $data);
    }
   
}

/* End of file cart.php */
/* Location: ./app/store/controllers/cart.php */
