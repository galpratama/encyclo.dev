<?php if (!defined('BASEPATH')) die();
class Store extends Main_Controller {

    public function index()
    {
        $data['view']  = 'store';

        $this->load->view('template', $data);
    }
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
