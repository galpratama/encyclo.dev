<?php if (!defined('BASEPATH')) die();
class Banners extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('banners_model');
    }


    public function index()
    {
        $data['view']  = 'banners';
        $data['title']  = 'Banners';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['banners'] = $this->banners_model->banners_get();

        $this->load->view('template', $data);
    }

    public function insert()
    {
        $data['title'] = $this->input->post('title');
        $data['image'] = $this->input->post('image');
        $data['link'] = $this->input->post('link');
        $data['new_window'] = $this->input->post('new_window');

        // Explode data from time to array
        $time_array = explode("/",$this->input->post('time'));

        // assign to $data variable
        $data['enable_time'] = $time_array[0];
        $data['disable_time'] = $time_array[1];

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/banners/"; //set dir variable for future use

        $config = array(
            'upload_path'     => $dir,
            'upload_url'      => base_url()."uploads/banners/",
            'remove_spaces'   => TRUE,
            'quality'         => '90',
            'allowed_types'   => "gif|jpg|png|jpeg",
            'overwrite'       => FALSE
        );

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload("image")) // if upload failed
        {
            echo $this->upload->display_errors(); // then show errors
        }
        else
        {
            $finfo = $this->upload->data(); // get file

            // rename image file and assign it
            $file_name = $finfo['file_name'];
            $new_file_name = date("Ymdhis") . '-' . preg_replace('/[^A-Za-z0-9]/',"-", $data['title'])  . '.jpg';

            rename($dir . $file_name, $dir . $new_file_name);

            $data['image'] = $new_file_name;
        }

        // insert data
        $this->banners_model->banners_insert($data);

        // set a success message
       $this->session->set_flashdata('success','<b>Success!</b> Banner Succesfully Uploaded');

        redirect('nycadmin/banners');
    }

    public function update()
    {
        $data['id'] = $this->input->post('id');
        $data['title'] = $this->input->post('title');
        $data['image'] = $this->input->post('image');
        $data['link'] = $this->input->post('link');
        $data['new_window'] = $this->input->post('new_window');

        $id = $data['id'];

        // Explode data from time to array
        $time_array = explode("/",$this->input->post('time'));

        // assign to $data variable
        $data['enable_time'] = $time_array[0];
        $data['disable_time'] = $time_array[1];

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/banners/"; //set dir variable for future use

        $config = array(
            'upload_path'     => $dir,
            'upload_url'      => base_url()."uploads/banners/",
            'remove_spaces'   => TRUE,
            'quality'         => '90',
            'allowed_types'   => "gif|jpg|png|jpeg",
            'overwrite'       => FALSE
        );

        if (empty($_FILES['image']['name'])){ // if input image is empty

            // only update some field
            $data_input = array(
                'title' => $data['title'],
                'enable_time' => $data['enable_time'],
                'disable_time' => $data['disable_time'],
                'link' => $data['link'],
                'new_window' => $data['new_window']
            );

            /// update to db
            $this->banners_model->banners_update($data_input,$id);

        }

        else {

            $this->load->library('upload', $config); // load upload library

            if(!$this->upload->do_upload("image")) // if upload failed
            {
                echo $this->upload->display_errors(); // then show errors
            }
            else
            {
                $finfo = $this->upload->data(); // get file

                // rename image file and assign it
                $file_name = $finfo['file_name'];
                $new_file_name = date("Ymdhis") . '-' . preg_replace('/[^A-Za-z0-9]/',"-", $data['title'])  . '.jpg';

                rename($dir . $file_name, $dir . $new_file_name);

                $data['image'] = $new_file_name;
            }

            // get image name
            $query_get = $this->banners_model->banners_get_from_id($id); // get selected data from banners
            $query_result = $query_get->row();

            $image_path = $dir . $query_result->image;

            // delete it
            unlink($image_path);

            // update to db
            $this->banners_model->banners_update($data,$id);

        }

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Banner Succesfully Edited');

        redirect('nycadmin/banners');

    }

    public function update_sort()
    {

    }

    public function delete()
    {
        // get id from url
        $id = $this->uri->segment(4);

        // get image name
        $query_get = $this->banners_model->banners_get_from_id($id); // get selected data from banners
        $query_result = $query_get->row();

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/banners/";
        $image_path = $dir . $query_result->image;

        echo $image_path;
        unlink($image_path);

        // then delete it
        $this->banners_model->banners_delete($id);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Banner Succesfully Deleted');

        redirect('nycadmin/banners/');
    }

}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
