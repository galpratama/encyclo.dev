<?php if (!defined('BASEPATH')) die();
class Administrator extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('administrator_model');
    }


    public function index()
    {
        $data['view']  = 'administrator';
        $data['title']  = 'Administrator';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['administrator'] = $this->administrator_model->administrator_get();

        $this->load->view('template', $data);
    }

    public function insert()
    {
        $data['email'] = $this->input->post('email');
        $data['image'] = $this->input->post('image');
        $data['name_first'] = $this->input->post('name_first');
        $data['name_last'] = $this->input->post('name_last');
        $data['password'] = $this->input->post('password');

        // Explode data from time to array
        $time_array = explode("/",$this->input->post('time'));

        // assign to $data variable
        $data['enable_time'] = $time_array[0];
        $data['disable_time'] = $time_array[1];

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/administrator/"; //set dir variable for future use

        $config = array(
            'upload_path'     => $dir,
            'upload_url'      => base_url()."uploads/administrator/",
            'remove_spaces'   => TRUE,
            'quality'         => '90',
            'allowed_types'   => "gif|jpg|png|jpeg",
            'overwrite'       => FALSE
        );

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload("image")) // if upload failed
        {
            echo $this->upload->display_errors(); // then show errors
        }
        else
        {
            $finfo = $this->upload->data(); // get file

            // rename image file and assign it
            $file_name = $finfo['file_name'];
            $new_file_name = date("Ymdhis") . '-' . preg_replace('/[^A-Za-z0-9]/',"-", $data['email'])  . '.jpg';

            rename($dir . $file_name, $dir . $new_file_name);

            $data['image'] = $new_file_name;
        }

        // insert data
        $this->administrator_model->administrator_insert($data);

        // set a success message
       $this->session->set_flashdata('success','<b>Success!</b> Administrator Succesfully Uploaded');

        redirect('nycadmin/administrator');
    }

    public function update()
    {
        $data['id'] = $this->input->post('id');
        $data['email'] = $this->input->post('email');
        $data['image'] = $this->input->post('image');
        $data['name_first'] = $this->input->post('name_first');
        $data['name_last'] = $this->input->post('name_last');
        $data['access_rights'] = $this->input->post('access_rights');

        $id = $data['id'];

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/administrator/"; //set dir variable for future use

        $config = array(
            'upload_path'     => $dir,
            'upload_url'      => base_url()."uploads/administrator/",
            'remove_spaces'   => TRUE,
            'quality'         => '90',
            'allowed_types'   => "gif|jpg|png|jpeg",
            'overwrite'       => FALSE
        );

        if (empty($_FILES['image']['name'])){ // if input image is empty

            // only update some field
            $data_input = array(
                'email' => $data['email'],
                'name_first' => $data['name_first'],
                'name_last' => $data['name_last'],
                'password' => $data['password']
            );

            /// update to db
            $this->administrator_model->administrator_update($data_input,$id);

        }

        else {

            $this->load->library('upload', $config); // load upload library

            if(!$this->upload->do_upload("image")) // if upload failed
            {
                echo $this->upload->display_errors(); // then show errors
            }
            else
            {
                $finfo = $this->upload->data(); // get file

                // rename image file and assign it
                $file_name = $finfo['file_name'];
                $new_file_name = date("Ymdhis") . '-' . preg_replace('/[^A-Za-z0-9]/',"-", $data['name_first'])  . '.jpg';

                rename($dir . $file_name, $dir . $new_file_name);

                $data['image'] = $new_file_name;
            }

            // get image name
            $query_get = $this->administrator_model->administrator_get_from_id($id); // get selected data from administrator
            $query_result = $query_get->row();

            $image_path = $dir . $query_result->image;

            // delete it
            unlink($image_path);

            // update to db
            $this->administrator_model->administrator_update($data,$id);

        }

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Administrator Succesfully Edited');

        redirect('nycadmin/administrator');

    }

    public function update_sort()
    {

    }

    public function delete()
    {
        // get id from url
        $id = $this->uri->segment(4);

        // get image name
        $query_get = $this->administrator_model->administrator_get_from_id($id); // get selected data from administrator
        $query_result = $query_get->row();

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/administrator/";
        $image_path = $dir . $query_result->image;

        echo $image_path;
        unlink($image_path);

        // then delete it
        $this->administrator_model->administrator_delete($id);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Administrator Succesfully Deleted');

        redirect('nycadmin/administrator/');
    }

}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
