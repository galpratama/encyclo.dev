<?php if (!defined('BASEPATH')) die();
class Nycadmin extends Main_Controller {

    public function index()
	{
        $data['view']  = 'index';
        $data['title']  = 'Dashboard';
        $data['additional_header']  = FALSE;
        $data['additional_footer']  = FALSE;

        $this->load->view('template', $data);
	}
   
}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
