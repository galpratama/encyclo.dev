<?php if (!defined('BASEPATH')) die();
class Products extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('products_model');
        $this->load->model('changelog_model');
    }

    public function index()
    {
        $data['view']  = 'products';
        $data['title']  = 'Products';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['products_list'] = $this->products_model->products_list_get();
        $this->load->view('template', $data);
    }

    public function add_details()
    {
        $data['view']  = 'products_add_details';
        $data['title']  = 'Add Product Details';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['products_add_details'] = $this->products_model->products_get_from_id($this->uri->segment(4));

        $this->load->view('template', $data);
    }


    public function edit_details()
    {
        $data['view']  = 'products_edit_details';
        $data['title']  = 'Edit Product Details';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['products_pos'] = $this->products_model->products_get_from_id($this->uri->segment(4));
        $data['products_webstore'] = $this->products_model->products_details_get($this->uri->segment(4));
        $this->load->view('template', $data);
    }

    public function images()
    {
        $data['view']  = 'products_images';
        $data['title']  = 'Products Images';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['products_images'] = $this->products_model->products_images_get($this->uri->segment(4));
        $data['products_name'] = $this->products_model->products_get_from_id($this->uri->segment(4));

        $this->load->view('template', $data);
    }


    public function images_insert()
    {
        $data['image'] = $this->input->post('image');
        $data['r_produk_id'] = $this->input->post('r_produk_id');

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/products/"; //set dir variable for future use

        $config = array(
            'upload_path'     => $dir,
            'upload_url'      => base_url()."uploads/products/",
            'remove_spaces'   => TRUE,
            'quality'         => '90',
            'allowed_types'   => "gif|jpg|png|jpeg",
            'overwrite'       => FALSE
        );

        $this->load->library('upload', $config);

        if(!$this->upload->do_upload("image")) // if upload failed
        {
            echo $this->upload->display_errors(); // then show errors
        }
        else
        {
            $finfo = $this->upload->data(); // get file

            // rename image file and assign it
            $file_name = $finfo['file_name'];
            $new_file_name = date("Ymdhis") . '-' . $data['r_produk_id']  . '.jpg';

            rename($dir . $file_name, $dir . $new_file_name);

            $data['image'] = $new_file_name;
        }

        // insert data
        $this->products_model->products_images_insert($data);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Products Images Succesfully Uploaded');

        redirect('nycadmin/products/images/' . $data['r_produk_id']);
    }

    public function insert_details()
    {
        $data['r_produk_id'] = $this->input->post('r_produk_id');
        $data['enabled'] = $this->input->post('enabled');
        $data['sale_price'] = $this->input->post('sale_price');
        $data['seo_title'] = $this->input->post('seo_title');
        $data['seo_meta'] = $this->input->post('seo_meta');

        // insert data
        $this->products_model->products_insert_details($data);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Product Details Succesfully Uploaded');

        redirect('nycadmin/products');
    }


    public function update_details()
    {
        $id = $this->input->post('r_produk_id');
        $data['sale_price'] = $this->input->post('sale_price');
        $data['seo_title'] = $this->input->post('seo_title');
        $data['seo_meta'] = $this->input->post('seo_meta');

        /// update to db
        $this->products_model->products_details_update($data,$id);

        // store to changelog
        $changelog['changelog_type'] = 'update';
        $changelog['changelog_text'] = 'Products <strong>' . $this->input->post('nmproduk') . '</strong> Successfully Edited';

        $this->changelog_model->changelog_insert($changelog);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Products <strong>' . $this->input->post('nmproduk') . '</strong> Succesfully Edited');

        redirect('nycadmin/products');

    }

    public function update_status_product()
    {
        $id = $this->uri->segment(5);
        $data['enabled'] = $this->uri->segment(4);

        /// update to db
        $this->products_model->products_details_update($data,$id);

        // store to changelog
        $changelog['changelog_type'] = 'update';
        $changelog['changelog_text'] = 'Products <strong>' . $this->input->post('nmproduk') . '</strong> Successfully Edited';

        $this->changelog_model->changelog_insert($changelog);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Products <strong>' . $this->input->post('nmproduk') . '</strong> Succesfully Edited');

        redirect('nycadmin/products');

    }

    public function images_delete()
    {
        // get id from url
        $id = $this->uri->segment(4);
        $product_id = $this->uri->segment(5);

        // get image name
        $query_get = $this->products_model->products_images_get_from_id($id); // get selected data from banners
        $query_result = $query_get->row();

        $dir = dirname($_SERVER["SCRIPT_FILENAME"])."/uploads/products/";
        $image_path = $dir . $query_result->image;

        echo $image_path;
        unlink($image_path);

        // then delete it
        $this->products_model->products_images_delete($id);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Product Images Succesfully Deleted');

        redirect('nycadmin/products/images/'. $product_id);
    }

    public function delete_details()
    {
        // get id from url
        $id = $this->uri->segment(4);

        // then delete it
        $this->products_model->products_details_delete($id);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Product Details Succesfully Deleted');

        redirect('nycadmin/products');
    }


}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
