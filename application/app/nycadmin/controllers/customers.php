<?php if (!defined('BASEPATH')) die();
class Customers extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('customers_model');
    }


    public function index()
    {
        $data['view']  = 'customers';
        $data['title']  = 'Customers';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['customers_list'] = $this->customers_model->customers_get();

        $this->load->view('template', $data);
    }
}