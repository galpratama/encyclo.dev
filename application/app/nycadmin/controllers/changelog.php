<?php if (!defined('BASEPATH')) die();
class Changelog extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('changelog_model');
    }


    public function index()
    {
        $data['view']  = 'changelog';
        $data['title']  = 'Changelog';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['featured'] = $this->changelog_model->changelog_get();

        $this->load->view('template', $data);
    }
}