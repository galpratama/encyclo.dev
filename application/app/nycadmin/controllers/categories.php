<?php if (!defined('BASEPATH')) die();
class Categories extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('categories_model');
    }


    public function index()
    {
        $data['view']  = 'categories';
        $data['title']  = 'Customers';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['categories_list'] = $this->categories_model->categories_get();

        $this->load->view('template', $data);
    }
}