<?php if (!defined('BASEPATH')) die();
class Pages extends Main_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('pages_model');
        $this->load->model('changelog_model');
    }


    public function index()
    {
        $data['view']  = 'pages';
        $data['title']  = 'Pages';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['pages'] = $this->pages_model->pages_get();

        $this->load->view('template', $data);
    }

    public function add()
    {
        $data['view']  = 'pages_add';
        $data['title']  = 'Add Pages';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $data['pages'] = $this->pages_model->pages_get();

        $this->load->view('template', $data);
    }

    public function edit()
    {
        $data['view']  = 'pages_edit';
        $data['title']  = 'Edit Pages';
        $data['additional_header']  = TRUE;
        $data['additional_footer']  = TRUE;

        $id = $this->uri->segment(4);

        $data['pages_edit'] = $this->pages_model->pages_get_from_id($id);

        $this->load->view('template', $data);
    }

    public function insert()
    {
        $data['title'] = $this->input->post('title');
        $data['title_link'] = $this->input->post('title_link');
        $data['slug'] = $this->input->post('slug');
        $data['content'] = $this->input->post('content');
        $data['seo_title'] = $this->input->post('seo_title');
        $data['seo_meta'] = $this->input->post('seo_meta');

        // insert data
        $this->pages_model->pages_insert($data);

        // store to changelog
        $changelog['changelog_type'] = 'insert';
        $changelog['changelog_text'] = 'Pages <strong>' . $data['title'] . '</strong> Successfully Added';

        $this->changelog_model->changelog_insert($changelog);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Pages <strong>' . $data['title'] . '</strong> Successfully Added');

        redirect('nycadmin/pages');
    }

    public function insert_link()
    {
        $data['title'] = $this->input->post('title');
        $data['url'] = $this->input->post('url');
        $data['parent_id'] = $this->input->post('parent_id');

        // insert data
        $this->pages_model->pages_insert($data);

        // store to changelog
        $changelog['changelog_type'] = 'insert';
        $changelog['changelog_text'] = 'Links <strong>' . $data['title'] . '</strong> Successfully Added';

        $this->changelog_model->changelog_insert($changelog);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Links  <strong>' . $data['title'] . '</strong> Successfully Added');

        redirect('nycadmin/pages');
    }

    public function update()
    {
        $id = $this->input->post('id');

        $data = array(
            'title' => $this->input->post('title'),
            'title_link' => $this->input->post('title_link'),
            'slug' => $this->input->post('slug'),
            'content' => $this->input->post('content'),
            'seo_title' => $this->input->post('seo_title'),
            'seo_meta' => $this->input->post('seo_meta')
        );

        /// update to db
        $this->pages_model->pages_update($data,$id);

        // store to changelog
        $changelog['changelog_type'] = 'update';
        $changelog['changelog_text'] = 'Pages <strong>' . $data['title'] . '</strong> Successfully Edited';

        $this->changelog_model->changelog_insert($changelog);


        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Pages <strong>' . $data['title'] . '</strong> Succesfully Edited');

        redirect('nycadmin/pages');

    }

    public function delete()
    {
        // get id from url
        $id = $this->uri->segment(4);

        // get name
        $pages_get_name = $this->pages_model->pages_get_from_id($id);
        $pages_get_name_result = $pages_get_name->row();

        // then delete it
        $this->pages_model->pages_delete($id);

        // store to changelog
        $changelog['changelog_type'] = 'delete';
        $changelog['changelog_text'] = 'Pages/Link <strong>' .  $pages_get_name_result->title .'</strong> Successfully Deleted';

        $this->changelog_model->changelog_insert($changelog);

        // set a success message
        $this->session->set_flashdata('success','<b>Success!</b> Pages/Link <strong>' .  $pages_get_name_result .'</strong> Succesfully Deleted');

        redirect('nycadmin/pages');
    }
}