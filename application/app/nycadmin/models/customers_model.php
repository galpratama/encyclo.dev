<?php

class Customers_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->pos = $this->load->database('pos', TRUE); //load pos databasee
    }

    public function customers_get()
    {
        $this->pos->select('*');
        $this->pos->from('erp.r_bp');
        $this->pos->where('r_bpgrup_id', '1000000'); //select dummy
        $query = $this->pos->get();
        return $query;
    }

}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 