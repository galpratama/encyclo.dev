<?php

class Pages_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee
    }

    public function pages_get()
    {
        $query = $this->webstore->get('pages'); // get all data from pages
        return $query;
    }

    public function pages_get_from_id($id)
    {
        $query = $this->webstore->get_where('pages',array('id' => $id)); // get all data from pages
        return $query;
    }

    public function pages_insert($data)
    {
        $this->webstore->insert('pages', $data); // insert into db
        return;
    }

    public function pages_update($data,$id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->update('pages',$data);
        return;
    }

    public function pages_delete($id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->delete('pages');
        return;
    }


}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 