<?php

class Featured_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee
    }

    public function featured_get()
    {
        $query = $this->webstore->get('featured'); // get all data from featured
        return $query;
    }

    public function featured_get_from_id($id)
    {
        $query = $this->webstore->get_where('featured',array('id' => $id)); // get all data from featured
        return $query;
    }

    public function featured_insert($data)
    {
        $this->webstore->insert('featured', $data); // insert into db
        return;
    }

    public function featured_update($data,$id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->update('featured',$data);
        return;
    }

    public function featured_sort_update()
    {

    }

    public function featured_delete($id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->delete('featured');
        return;
    }


}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 