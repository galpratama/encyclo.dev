<?php

class Administrator_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee
    }

    public function administrator_get()
    {
        $query = $this->webstore->get('administrator'); // get all data from administrator
        return $query;
    }

    public function administrator_get_from_id($id)
    {
        $query = $this->webstore->get_where('administrator',array('id' => $id)); // get all data from administrator
        return $query;
    }

    public function administrator_insert($data)
    {
        $this->webstore->insert('administrator', $data); // insert into db
        return;
    }

    public function administrator_update($data,$id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->update('administrator',$data);
        return;
    }

    public function administrator_sort_update()
    {

    }

    public function administrator_delete($id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->delete('administrator');
        return;
    }


}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 