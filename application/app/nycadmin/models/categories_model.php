<?php

class Categories_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->pos = $this->load->database('pos', TRUE); //load pos databasee
    }

    public function categories_get()
    {
        $query = $this->pos->get('erp.r_produkkategori');
        return $query;
    }

}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 