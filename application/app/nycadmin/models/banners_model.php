<?php

class Banners_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee
    }

    public function banners_get()
    {
        $query = $this->webstore->get('banners'); // get all data from banners
        return $query;
    }

    public function banners_get_from_id($id)
    {
        $query = $this->webstore->get_where('banners',array('id' => $id)); // get all data from banners
        return $query;
    }

    public function banners_insert($data)
    {
        $this->webstore->insert('banners', $data); // insert into db
        return;
    }

    public function banners_update($data,$id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->update('banners',$data);
        return;
    }

    public function banners_sort_update()
    {

    }

    public function banners_delete($id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->delete('banners');
        return;
    }


}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 