<?php

class Changelog_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee
    }

    public function changelog_get()
    {
        $query = $this->webstore->get('changelog'); // get all data from changelog
        return $query;
    }

    public function changelog_get_from_id($id)
    {
        $query = $this->webstore->get_where('changelog',array('id' => $id)); // get all data from changelog
        return $query;
    }

    public function changelog_insert($data)
    {
        $this->webstore->insert('changelog', $data); // insert into db
        return;
    }



}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 