<?php

class Products_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->pos = $this->load->database('pos', TRUE); //load pos databasee
        $this->webstore = $this->load->database('webstore', TRUE); //load pos databasee
    }

    public function products_insert_details($data)
    {
        $this->webstore->insert('product_details', $data); // insert into db
        return;
    }

    public function products_images_insert($data)
    {
        $this->webstore->insert('product_image', $data); // insert into db
        return;
    }

    public function products_details_get()
    {
        $query = $this->webstore->get('product_details');
        return $query;
    }

    public function products_list_get()
    {
        $this->pos->select('r_produk_id, r_brand_id, r_produkkategori_id,nmproduk,kdproduk,hpj');
        $this->pos->from('erp.r_produk');
        $this->pos->where('r_brand_id', '1001108'); //select dummy
        $query = $this->pos->get();
        return $query;
    }

    public function products_images_get($id)
    {
        $this->webstore->select('*');
        $this->webstore->from('product_image');
        $this->webstore->where('r_produk_id', $id);
        $query = $this->webstore->get();
        return $query;
    }

    public function products_get_from_id($id)
    {
        $query = $this->pos->get_where('erp.r_produk',array('r_produk_id' => $id)); // get all data from products
        return $query;
    }

    public function products_get_details_from_id()
    {
        $query = $this->pos->get_where('product_details',array('r_produk_id' => $id)); // get all data from products
        return $query;
    }

    public function products_get_name_from_id($id)
    {
        $query = $this->webstore->get_where('product_image',array('r_produk_id' => $id)); // get all data from products
        return $query;
    }

    public function products_images_get_from_id($id)
    {
        $query = $this->webstore->get_where('product_image',array('id' => $id)); // get all data from products
        return $query;
    }

    public function products_images_delete($id)
    {
        $this->webstore->where('id',$id);
        $this->webstore->delete('product_image');
        return;
    }

    public function products_details_delete($id)
    {
        $this->webstore->where('r_produk_id',$id);
        $this->webstore->delete('product_details');
        return;
    }

    public function products_details_update($data,$id)
    {
        $this->webstore->where('r_produk_id',$id);
        $this->webstore->update('product_details',$data);
        return;
    }


}

/**
 * Created by PhpStorm.
 * User: Galih Pratama
 * Date: 19/08/2014
 * Time: 13:50
 */ 