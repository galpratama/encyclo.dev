<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Pages
            <small>Content Management System</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-laptop"></i> CMS</a></li>
            <li><a href="#"> Pages</a></li>
            <li class="active">Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_content" data-toggle="tab">Content</a></li>
                    <li><a href="#tab_attributes" data-toggle="tab">Attributes</a></li>
                    <li><a href="#tab_seo" data-toggle="tab">SEO</a></li>
                    <li class="pull-right header">
                        <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/pages/insert" method="post">
                        <button type="submit" class="btn btn-success btn-md"><i class="fa fa-save"></i> Save Changes</button>
                        <button type="reset" class="btn btn-warning btn-md"><i class="fa fa-refresh"></i> Reset</button>
                        <button type="button" onclick="goBack()" class="btn btn-danger btn-md"><i class="fa fa-trash-o"></i> Discard</button>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_content">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Example: My Awesome Pages">
                        </div>
                        <div class="form-group">
                            <label for="">Parent</label>
                            <select name="parent_id" id="" class="form-control">
                                <option value="0">Top Level</option>
                                <?php foreach ($pages->result() as $pages_parent_id){ ?>
                                    <option value="<?php echo $pages_parent_id->id ?>">
                                        <?php echo $pages_parent_id->title;?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="new_window" value="1"> Open in New Tab / Window
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label>
                            <textarea name="content" id="content">
                            </textarea>
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_attributes">
                        <div class="form-group">
                            <label for="">Title in Links</label>
                            <input type="text" name="title_link" class="form-control" placeholder="Example: Awesome Pages">
                        </div>
                        <div class="form-group">
                            <label for="">Slug (URL Name)</label>
                            <input type="text" name="slug" class="form-control" placeholder="Example: contact">
                        </div>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_seo">
                        <div class="form-group">
                            <label for="">SEO Title</label>
                            <input type="text" name="seo_title" class="form-control" placeholder="Example: My Awesome Pages">
                        </div>
                        <div class="form-group">
                            <label for="">SEO Meta</label><br/>
                            <textarea name="seo_meta" class="form-control" id="meta" width="100%"></textarea>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </form>
        </div><!-- nav-tabs-custom -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->
