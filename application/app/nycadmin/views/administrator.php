<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Administrator
            <small>System</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-laptop"></i> System</a></li>
            <li class="active">Administrator</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button  class="btn btn-primary btn-md" data-toggle="modal" data-target="#administrator_insert" title="Add Administrator / Slider on Home Page"><i class="fa fa-plus"></i> Add Administrator</button>
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#administrator_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-laptop"></i>
            <h3 class="box-title">
                Administrator List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="administrator" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="25%">First Name</th>
        <th width="25%">Last Name</th>
        <th width="20%">Email</th>
        <th width="20%">Access Rights</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($administrator->result() as $administrator_view){ ?>
        <tr>
            <td><?php echo $administrator_view->name_first ?></td>
            <td><?php echo $administrator_view->name_last ?></td>
            <td><?php echo $administrator_view->email ?></td>
            <td><?php
                if ($administrator_view->access_rights === '1'){
                    echo "Full Administrator";
                }
                ?></td>
            <td class="td-action">
                <button class="btn btn-success btn-table" data-toggle="modal" data-target="#administrator_edit<?php echo $administrator_view->id ?>"><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-warning btn-table" data-toggle="modal" data-target="#administrator_password<?php echo $administrator_view->id ?>"><i class="fa fa-key"></i> Password</button>
                <button class="btn btn-danger btn-table" data-toggle="modal" data-target="#administrator_delete<?php echo $administrator_view->id ?>"><i class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Access Rights</th>
        <th>Action</th>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->

<!-- Add Administrator Modal Form -->
<div class="modal fade" id="administrator_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Administrator</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/administrator/insert" method="post">
                    <div class="form-group float-label-control">
                        <label for="">First Name</label>
                        <input type="text" name="name_first" class="form-control" placeholder="First Name">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Last Name</label>
                        <input type="text" name="name_last" class="form-control" placeholder="Last Name">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Retype Password</label>
                        <input type="password" name="password_retype" class="form-control" placeholder="Retype Password">
                    </div>
                    <div class="form-group">
                        <label for="">Profile Picture</label>
                        <input type="file" name="image" class="file" placeholder="Administrator Image" data-show-upload="false">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes">
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Delete Administrator Modal Form -->
<?php foreach ($administrator->result() as $administrator_delete){ ?>
    <div class="modal fade" id="administrator_delete<?php echo $administrator_delete->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Administrator</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete <strong>"<?php echo $administrator_delete->name_first ?>"</strong>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url()?>nycadmin/administrator/delete/<?php echo $administrator_delete->id ?>" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Edit Administrator Modal Form -->
<?php foreach ($administrator->result() as $administrator_edit){ ?>
    <div class="modal fade" id="administrator_edit<?php echo $administrator_edit->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Administrator <strong>"<?php echo $administrator_edit->name_first ?>"</strong></h4>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/administrator/update" method="post">
                        <div class="form-group float-label-control">
                            <label for="">First Name</label>
                            <input type="text" name="name_first" class="form-control" placeholder="First Name" value="<?php echo $administrator_edit->name_first ?>">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">Last Name</label>
                            <input type="text" name="name_last" class="form-control" placeholder="Last Name" value="<?php echo $administrator_edit->name_last ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Existing Administrator Image</label>
                            <img src="<?php echo base_url('uploads/administrator') . '/' . $administrator_edit->image; ?>" class="thumbnail col-xs-12"/>
                        </div>
                        <div class="form-group">
                            <label for="">Change Administrator Image</label>
                            <input type="file" name="image" class="file" placeholder="Administrator Image" data-show-upload="false">
                        </div>
                        <input type="hidden" name="id" value="<?php echo $administrator_edit->id ?>">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<!-- Edit Password Modal Form -->
<?php foreach ($administrator->result() as $administrator_edit){ ?>
    <div class="modal fade" id="administrator_password<?php echo $administrator_edit->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Administrator Password <strong>"<?php echo $administrator_edit->name_first . ' ' . $administrator_edit->name_last ?>"</strong></h4>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/administrator/update" method="post">
                        <div class="form-group float-label-control">
                            <label for="">Old Password</label>
                            <input type="text" name="password_old" class="form-control" placeholder="Your Old Password">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">New Password</label>
                            <input type="text" name="password" class="form-control" placeholder="Your New Password">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">Retype New Password</label>
                            <input type="text" name="password_retype" class="form-control" placeholder="Retype New Password">
                        </div>
                        <input type="hidden" name="id" value="<?php echo $administrator_edit->id ?>">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>