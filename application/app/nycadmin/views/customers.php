<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers
            <small>Lists</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-user"></i> Customers</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="#" class="btn btn-primary btn-md" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-plus-circle"></i> Add Members</a>
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#customers_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-archive"></i>
            <h3 class="box-title">
                Customers List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="customers" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10%">ID</th>
        <th width="10%">Code</th>
        <th width="35%">Name</th>
        <th width="10%">Date Registered</th>
        <th width="10%">BP Type</th>
        <th width="25%">Action</th>
    </tr>
    </thead>
    <tbody>
        <?php
        foreach ($customers_list->result() as $customers_list_view){
        ?>
        <tr>
            <td><?php echo $customers_list_view->r_bp_id ?></td>
            <td><?php echo $customers_list_view->kdbp ?></td>
            <td><?php echo $customers_list_view->nmbp ?></td>
            <td><?php echo $customers_list_view->tgldaftar ?></td>
            <td><?php echo $customers_list_view->jenis_bp ?></td>
            <td>
                <a class="btn btn-success" href="#"><i class="fa fa-eye"></i> View</a>
                <a class="btn btn-primary" href="#"><i class="fa fa-edit"></i> Edit</a>
                <a class="btn btn-danger" href="#"><i class="fa fa-trash-o"></i> Delete</a>
            </td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>Code</th>
        <th>Date Registered</th>
        <th>BP Type</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->