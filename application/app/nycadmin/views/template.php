<?php $this->load->view('include/header'); // CSS & Meta Tag ?>
<?php if ($additional_header == TRUE) {
    $this->load->view('/include/header_'. $view); // Additional Header
}?>
<?php $this->load->view('/include/top'); // Top of HTML ?>
<?php $this->load->view('include/nav'); // Navigation ?>
<?php $this->load->view('include/sidenav'); // Side Navigation ?>
<?php $this->load->view($view); // Load View ?>
<?php $this->load->view('/include/footer'); // Javascript ?>
<?php if ($additional_footer == TRUE) {
    $this->load->view('/include/footer_'. $view); // Additional Footer
}?>
<?php $this->load->view('/include/end'); // End of HTML ?>