<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pages
            <small>Content Management System</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-laptop"></i> CMS</a></li>
            <li class="active">Pages</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="<?php echo base_url('nycadmin/pages/');?>/add" class="btn btn-primary btn-md" title="Add Pages"><i class="fa fa-plus"></i> Add Pages</a>
                <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#links_insert"><i class="fa fa-external-link"></i> Add Links</button>
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#pages_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-laptop"></i>
            <h3 class="box-title">
                Pages List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="pages" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="30%">Title</th>
        <th width="25%">Title in Links</th>
        <th width="15%">SEO Title</th>
        <th width="20%">Content</th>
        <th width="10%">Type</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($pages->result() as $pages_view){ ?>
        <tr>
            <td><strong><?php echo $pages_view->title; ?></strong></td>
            <td><strong><?php echo $pages_view->title_link; ?></strong></td>
            <td><?php echo $pages_view->seo_title; ?></td>
            <td><?php echo substr(strip_tags($pages_view->content),0,250); ?> [...]</td>
            <td>
                <?php
                    if ($pages_view->url != NULL) {
                        echo "Links";
                    }
                    else {
                        echo "Pages";
                    }
                ?>
            </td>
            <td class="td-action">
                <a href="<?php echo base_url('nycadmin/pages/');?>/edit/<?php echo $pages_view->id ?>" class="btn btn-success btn-table"><i class="fa fa-edit"></i> Edit</a>
                <button class="btn btn-danger btn-table" data-toggle="modal" data-target="#pages_delete<?php echo $pages_view->id ?>"><i class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>Title</th>
        <th>Title in Links</th>
        <th>SEO Title</th>
        <th>Content</th>
        <th>Type</th>
        <th>Action</th>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->


<!-- Add Links Modal Form -->
<div class="modal fade" id="links_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Links</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/pages/insert_link" method="post">
                    <div class="form-group float-label-control">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Title">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Link <small>(with http://)</small></label>
                        <input type="text" name="url" class="form-control" placeholder="Link (with http://)">
                    </div>
                    <div class="form-group">
                        <label for="">Parent</label>
                        <select name="parent_id" id="" class="form-control">
                            <option value="0">Top Level</option>
                            <?php foreach ($pages->result() as $pages_parent_id){ ?>
                                <option value="<?php echo $pages_parent_id->id ?>">
                                    <?php echo $pages_parent_id->title;?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="new_window" value="1"> Open in New Tab / Window
                        </label>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes">
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Pages Modal Form -->
<?php foreach ($pages->result() as $pages_delete){ ?>
    <div class="modal fade" id="pages_delete<?php echo $pages_delete->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Pages</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete <strong>"<?php echo $pages_delete->title ?>"</strong>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url()?>nycadmin/pages/delete/<?php echo $pages_delete->id ?>" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>