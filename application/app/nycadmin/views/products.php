<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Products
            <small>Catalog</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Catalog</a></li>
            <li class="active">Products</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#products_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-archive"></i>
            <h3 class="box-title">
                Products List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="products" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="5%">ID</th>
        <th width="15%">SKU</th>
        <th width="57%">Product Name</th>
        <th width="23%">Action</th>
    </tr>
    </thead>
    <tbody>
        <?php
        foreach ($products_list->result() as $products_list_view){
        ?>
        <tr>
            <td><?php echo $products_list_view->r_produk_id ?></td>
            <td><?php echo $products_list_view->kdproduk ?></td>
            <td><?php echo $products_list_view->nmproduk ?></td>
            <td>
                <?php
                // bad example again, call db on view
                $this->webstore = $this->load->database('webstore', TRUE); //load pos databasee
                $this->webstore->where('r_produk_id',$products_list_view->r_produk_id);
                $product_query = $this->webstore->get('product_details');
                $product_details = $product_query->row();
                ?>
                <?php if ($product_query->num_rows() > 0){ ?>
                    <div class="btn-group">
                        <a class="btn btn-warning" href="<?php echo base_url('nycadmin/products/edit_details/') . '/' . $products_list_view->r_produk_id ?>"><i class="fa fa-edit"></i> Edit Details</a>
                        <a data-toggle="dropdown" class="btn btn-warning dropdown-toggle" type="button"><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php if ($product_details->enabled == '1'){ ?>
                                <li><a href="<?php echo base_url('nycadmin/products/update_status_product/0/') . '/' . $products_list_view->r_produk_id ?>"><i class="fa fa-times-circle"></i>Disable in Store</a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo base_url('nycadmin/products/update_status_product/1/') . '/' . $products_list_view->r_produk_id ?>"><i class="fa fa-check-circle"></i>Enable in Store</a></li>
                            <?php } ?>
                            <li><a data-toggle="modal" href="#products_details_delete<?php echo $products_list_view->r_produk_id ?>"><i class="fa fa-trash-o"></i>Delete Details</a></li>
                        </ul>
                    </div>
                <?php } else {?>
                    <a class="btn btn-primary" href="<?php echo base_url('nycadmin/products/add_details/') . '/' . $products_list_view->r_produk_id ?>"><i class="fa fa-plus-circle"></i> Add Details</a>
                <?php } ?>
                <a class="btn btn-info" href="<?php echo base_url('nycadmin/products/images/') . '/' . $products_list_view->r_produk_id ?>"><i class="fa fa-picture-o"></i> Images</a>
            </td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>SKU</th>
        <th>Product Name</th>
        <th>Action</th>
    </tr>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->

<!-- Delete Products Details Modal Form -->
<?php foreach ($products_list->result() as $products_list_delete){ ?>
    <div class="modal fade" id="products_details_delete<?php echo $products_list_delete->r_produk_id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Products Details</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete "<?php echo $products_list_delete->nmproduk ?>" Details?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url()?>nycadmin/products/delete_details/<?php echo $products_list_delete->r_produk_id ?>" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>