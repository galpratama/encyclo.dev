<?php $pages_edit_result = $pages_edit->row();?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Pages
            <small>Content Management System</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-laptop"></i> CMS</a></li>
            <li><a href="#"> Pages</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Custom Tabs (Pulled to the right) -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_content" data-toggle="tab">Content</a></li>
                        <li><a href="#tab_attributes" data-toggle="tab">Attributes</a></li>
                        <li><a href="#tab_seo" data-toggle="tab">SEO</a></li>
                        <li class="pull-right header">
                            <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/pages/update" method="post">
                                <button type="submit" class="btn btn-success btn-md"><i class="fa fa-save"></i> Save Changes</button>
                                <button type="reset" class="btn btn-warning btn-md"><i class="fa fa-refresh"></i> Reset</button>
                                <button type="button" onclick="goBack()" class="btn btn-danger btn-md"><i class="fa fa-trash-o"></i> Discard</button>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_content">
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Example: My Awesome Pages" value="<?php echo $pages_edit_result->title ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Content</label>
                                <textarea name="content" id="content"><?php echo $pages_edit_result->content ?></textarea>
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_attributes">
                            <div class="form-group">
                                <label for="">Title in Links</label>
                                <input type="text" name="title_link" class="form-control" placeholder="Example: Awesome Pages" value="<?php echo $pages_edit_result->title_link ?>">
                            </div>
                            <div class="form-group">
                                <label for="">Slug (URL Name)</label>
                                <input type="text" name="slug" class="form-control" placeholder="Example: contact" value="<?php echo $pages_edit_result->slug ?>">
                            </div>
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_seo">
                            <div class="form-group">
                                <label for="">SEO Title</label>
                                <input type="text" name="seo_title" class="form-control" placeholder="Example: My Awesome Pages" value="<?php echo $pages_edit_result->seo_title ?>">
                            </div>
                            <div class="form-group">
                                <label for="">SEO Meta</label><br/>
                                <textarea name="seo_meta" class="form-control" id="meta" width="100%"><?php echo $pages_edit_result->seo_meta ?></textarea>
                            </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                    <input type="hidden" name="id" value="<?php echo $pages_edit_result->id ?>"/>
                    </form>
                </div><!-- nav-tabs-custom -->
            </div>
        </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->
