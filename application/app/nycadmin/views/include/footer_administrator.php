<!-- DATA TABES SCRIPT -->
<script src="<?php echo base_url('assets/backend')?>/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/backend')?>/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<!-- page script -->
<script type="text/javascript">
    $(function() {
        $('#administrator').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>

<!-- date-range-picker -->
<script src="<?php echo base_url('assets/backend')?>/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    //Date range picker
    $(document).ready(function() {
        $('input[id="daterange"]').daterangepicker(
            {
                format: 'YYYY-MM-DD',
                separator:'/'
            }
        );
    });
</script>

<!-- Bootstrap Image Lightbox-->
<script src="<?php echo base_url('assets/backend')?>/js/plugins/ekko-lightbox/ekko-lightbox.js" type="text/javascript"></script>
<script>
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
</script>

<!-- Bootstrap Fileinput -->
<script src="<?php echo base_url('assets/backend')?>/js/plugins/bootstrap-fileinput/fileinput.js" type="text/javascript"></script>
