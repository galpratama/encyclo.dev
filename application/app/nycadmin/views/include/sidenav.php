<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url('assets/backend')?>/img/avatar6.jpg" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>Hello, Galih</p>

                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="active">
                    <a href="<?php echo base_url('nycadmin/index')?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>CMS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('nycadmin/banners')?>"><i class="fa fa-angle-double-right"></i> Banners</a></li>
                        <li><a href="<?php echo base_url('nycadmin/pages')?>"><i class="fa fa-angle-double-right"></i> Pages</a></li>
                        <li><a href="<?php echo base_url('nycadmin/featured')?>"><i class="fa fa-angle-double-right"></i> Featured Image</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-archive"></i>
                        <span>Catalog</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('nycadmin/categories')?>"><i class="fa fa-angle-double-right"></i> Categories</a></li>
                        <li><a href="<?php echo base_url('nycadmin/products')?>"><i class="fa fa-angle-double-right"></i> Products</a></li>
<!--                        <li><a href="--><?php //echo base_url('nycadmin/digital')?><!--"><i class="fa fa-angle-double-right"></i> Digital Products</a></li>-->
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('nycadmin/orders')?>">
                        <i class="fa fa-shopping-cart"></i> <span>Orders</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Customers</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('nycadmin/customers')?>"><i class="fa fa-angle-double-right"></i> Customers</a></li>
                        <li><a href="<?php echo base_url('nycadmin/guest')?>"><i class="fa fa-angle-double-right"></i> Guest Customers</a></li>
                        <!--                        <li><a href="--><?php //echo base_url('nycadmin/digital')?><!--"><i class="fa fa-angle-double-right"></i> Digital Products</a></li>-->
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('nycadmin/reports')?>">
                        <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('nycadmin/coupon')?>">
                        <i class="fa fa-ticket"></i> <span>Coupons</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span>System</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('nycadmin/settings')?>"><i class="fa fa-gear"></i> Settings</a></li>
                        <li><a href="<?php echo base_url('nycadmin/administrator')?>"><i class="fa fa-user-md"></i> Administrator</a></li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>