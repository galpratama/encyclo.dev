
<!-- header logo: style can be found in header.less -->
<header class="header">
<a href="index.html" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    Shafira Encyclo
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-right">
<ul class="nav navbar-nav">
<!-- Messages: style can be found in dropdown.less-->
<li class="dropdown"><a href="<?php echo base_url();?>" target="_blank"><i class="fa fa-external-link"></i> Store</a></li>
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-shopping-cart"></i> Orders
        <span class="label label-success">1</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have 1 new order</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li><!-- start message -->
                    <a href="#">
                        <i class="fa fa-shopping-cart success"></i>
                        Orders from <strong>Galih Pratama</strong>
                    </a>
                </li><!-- end message -->
            </ul>
        </li>
        <li class="footer"><a href="#">See All New Orders</a></li>
    </ul>
</li>
<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-warning"></i> Changes
    </a>
    <ul class="dropdown-menu">
        <li class="header">Changes</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php
                // this is bad practice of loading db query on view -_-

                $this->webstore = $this->load->database('webstore', TRUE); //load webstore databasee

                // get changelog
                $this->webstore->order_by('id','desc');
                $this->db->limit(10);
                $changelog_query = $this->webstore->get('changelog');

                foreach ($changelog_query->result() as $changelog_result){ ?>
                <li>
                    <a href="#">
                    <?php
                    // set status icon
                    if($changelog_result->changelog_type === 'insert'){
                        ?> <i class="fa fa-plus-square success"></i> <?php
                    } else if($changelog_result->changelog_type === 'update') {
                        ?> <i class="fa fa-edit info"></i> <?php
                    } else if($changelog_result->changelog_type === 'delete'){
                        ?> <i class="fa fa-trash-o danger"></i> <?php
                    }
                    // echo changelog text
                    echo $changelog_result->changelog_text;
                    ?>
                    </a>
                </li>
                <?php } ?>

            </ul>
        </li>
        <li class="footer"><a href="<?php echo base_url()?>nycadmin/changelog">View all</a></li>
    </ul>
</li>
<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i>
        <span><i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header bg-light-blue">
            <img src="<?php echo base_url('assets/backend'); ?>/img/avatar6.jpg" class="img-circle" alt="User Image" />
            <p>
                Galih Pratama
                <br/><small>galpratama</small>
            </p>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Edit Account</a>
            </div>
            <div class="pull-right">
                <a href="#" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</nav>
</header>