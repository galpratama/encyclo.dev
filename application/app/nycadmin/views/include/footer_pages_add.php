<!-- TinyMCE -->
<script src="<?php echo base_url('assets/backend')?>/js/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>

<script>
    tinymce.init({
        selector: "textarea#content",
        theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor jbimages"
        ],
        content_css: "css/content.css",
        relative_urls: false,
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media fullpage | forecolor backcolor emoticons"
    });
</script>