<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Banners
            <small>Content Management System</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-laptop"></i> CMS</a></li>
            <li class="active">Banners</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button  class="btn btn-primary btn-md" data-toggle="modal" data-target="#banners_insert" title="Add Banners / Slider on Home Page"><i class="fa fa-plus"></i> Add Banners</button>
                <button class="btn btn-warning btn-md" data-toggle="tooltip" title="Update Priority of Banner on Homepage"><i class="fa fa-refresh"></i> Update Banner Sort</button>
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#banners_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>

            </div><!-- /. tools -->

            <i class="fa fa-laptop"></i>
            <h3 class="box-title">
                Banners List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="banners" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="5%">Sort</th>
        <th width="35%">Title</th>
        <th width="15%">Enable Date</th>
        <th width="15%">Disable Date</th>
        <th width="20%">Preview</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($banners->result() as $banners_view){ ?>
        <tr>
            <td>
                <input type="text" name="<?php echo $banners_view->id ?>" class="form-control" value="<?php echo $banners_view->position ?>" required="required">
            </td>
            <td><?php echo $banners_view->title ?></td>
            <td><?php $tanggal = date('j F Y', strtotime($banners_view->enable_time)); echo $tanggal;  ?></td>
            <td><?php $tanggal = date('j F Y', strtotime($banners_view->disable_time)); echo $tanggal;  ?></td>
            <td>
                <a href="<?php echo base_url('uploads/banners') . '/' . $banners_view->image; ?>" data-toggle="lightbox">
                    <img src="<?php echo base_url('uploads/banners') . '/' . $banners_view->image; ?>" alt="<?php echo $banners_view->title ?>" class="banners_preview"/>
                </a>
            </td>
            <td class="td-action">
                <button class="btn btn-success btn-table" data-toggle="modal" data-target="#banners_edit<?php echo $banners_view->id ?>"><i class="fa fa-edit"></i> Edit</button>
                <button class="btn btn-danger btn-table" data-toggle="modal" data-target="#banners_delete<?php echo $banners_view->id ?>"><i class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>Sort</th>
        <th>Title</th>
        <th>Enable Date</th>
        <th>Disable Date</th>
        <th>Preview</th>
        <th>Action</th>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->

<!-- Add Banners Modal Form -->
<div class="modal fade" id="banners_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Banners</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/banners/insert" method="post">
                    <div class="form-group float-label-control">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Title">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">Link <small>(with http://)</small></label>
                        <input type="text" name="link" class="form-control" placeholder="Link (with http://)">
                    </div>
                    <div class="form-group">
                        <label for="">Banner Image</label>
                        <input type="file" name="image" class="file" placeholder="Banner Image" data-show-upload="false">
                    </div>
                    <!-- Date range -->
                    <div class="form-group">
                        <label>Date Active:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="time" class="form-control pull-right" id="daterange"/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="new_window" value="1"> Open in New Tab / Window
                        </label>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes">
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Delete Banners Modal Form -->
<?php foreach ($banners->result() as $banners_delete){ ?>
    <div class="modal fade" id="banners_delete<?php echo $banners_delete->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Banners</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete <strong>"<?php echo $banners_delete->title ?>"</strong>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url()?>nycadmin/banners/delete/<?php echo $banners_delete->id ?>" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Edit Banners Modal Form -->
<?php foreach ($banners->result() as $banners_edit){ ?>
    <div class="modal fade" id="banners_edit<?php echo $banners_edit->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Banners <strong>"<?php echo $banners_edit->title ?>"</strong></h4>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/banners/update" method="post">
                        <div class="form-group float-label-control">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Title" value="<?php echo $banners_edit->title ?>">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">Link <small>(with http://)</small></label>
                            <input type="text" name="link" class="form-control" placeholder="Link (with http://)" value="<?php echo $banners_edit->link ?>">
                        </div>
                        <div class="form-group">
                            <label for="">Existing Banner Image</label>
                            <img src="<?php echo base_url('uploads/banners') . '/' . $banners_edit->image; ?>" alt="<?php echo $banners_edit->title ?>" class="thumbnail col-xs-12"/>
                        </div>
                        <div class="form-group">
                            <label for="">Change Banner Image</label>
                            <input type="file" name="image" class="file" placeholder="Banner Image" data-show-upload="false">
                        </div>
                        <!-- Date range -->
                        <div class="form-group">
                            <label>Date Active:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="time" class="form-control pull-right" id="daterange" value="<?php echo $banners_edit->enable_time . '/' . $banners_edit->disable_time  ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="new_window" value="<?php echo $banners_edit->new_window ?>"> Open in New Tab / Window
                            </label>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $banners_edit->id ?>">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes">
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>