<?php $products_name_result = $products_name->row();?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Products
            <small>Catalog</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Catalog</a></li>
            <li class="active">Products</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="<?php echo base_url('nycadmin/products/'); ?>" class="btn btn-warning btn-md"><i class="fa fa-arrow-left"></i> Go Back</a>
                <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#products_images_insert"><i class="fa fa-plus"></i> Add Products Images</button>
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#products_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-archive"></i>
            <h3 class="box-title">
                Products Images for "<?php echo $products_name_result->nmproduk ?>"
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="products" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>No.</th>
        <th>Images</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        foreach ($products_images->result() as $products_list_view){
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td>
                <a href="<?php echo base_url('uploads/products/') . '/' . $products_list_view->image ?>" data-toggle="lightbox">
                    <img src="<?php echo base_url('uploads/products/') . '/' . $products_list_view->image ?>" alt="" class="thumbnail col-xs-6"/></td>
                </a>
            <td>
                <button class="btn btn-danger btn-table" data-toggle="modal" data-target="#products_images_delete<?php echo $products_list_view->id ?>"><i class="fa fa-trash-o"></i> Delete</button>
            </td>
        </tr>
    <?php $count++; }?>
    </tbody>
    <tfoot>
    <tr>
        <th>No.</th>
        <th>Images</th>
        <th>Action</th>
    </tr>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->


<!-- Add Products Images Modal Form -->
<div class="modal fade" id="products_images_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Products Images</h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/products/images_insert" method="post">
                    <div class="form-group">
                        <label for="">Banner Image</label>
                        <input type="file" name="image" class="file" placeholder="Banner Image" data-show-upload="false">
                    </div>
                    <input type="hidden" name="r_produk_id" value="<?php echo $products_name_result->r_produk_id ?>"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes">
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Delete Products Images Modal Form -->
<?php foreach ($products_images->result() as $products_images_delete){ ?>
    <div class="modal fade" id="products_images_delete<?php echo $products_images_delete->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Delete Products Images</h4>
                </div>
                <div class="modal-body">
                    <p>Do you want to delete this image</strong>?</p>
                    <p>
                        <img src="<?php echo base_url('uploads/products/') . '/' . $products_images_delete->image ?>" alt="" class="thumbnail col-xs-12"/></td>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="<?php echo base_url()?>nycadmin/products/images_delete/<?php echo $products_images_delete->id ?>/<?php echo $products_images_delete->r_produk_id ?>" class="btn btn-danger">
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php } ?>