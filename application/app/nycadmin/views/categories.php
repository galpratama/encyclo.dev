<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categories
            <small>Catalog</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Catalog</a></li>
            <li class="active">Categories</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button class="btn btn-success btn-md" data-toggle="tooltip" data-target="#categories_help" title="(Coming Soon)Want Help? Click here!"><i class="fa fa-question-circle"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-archive"></i>
            <h3 class="box-title">
                Categories List
            </h3>
        </div>
    <div class="box-body table-responsive">
    
        <?php if ($this->session->flashdata('success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('success');?>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('error');?>
            </div>
        <?php } ?>

    <table id="categories" class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="10%">ID</th>
        <th width="45%">Product Category Code</th>
        <th width="45%">Product Category Name</th>
    </tr>
    </thead>
    <tbody>
        <?php
        foreach ($categories_list->result() as $categories_list_view){
        ?>
        <tr>
            <td><?php echo $categories_list_view->r_produkkategori_id ?></td>
            <td><?php echo $categories_list_view->kdprodukkategori ?></td>
            <td><?php echo $categories_list_view->nmprodukkategori ?></td>
        </tr>
    <?php }?>
    </tbody>
    <tfoot>
    <tr>
        <th>ID</th>
        <th>SKU</th>
        <th>Product Name</th>
    </tr>
    </tr>
    </tfoot>
    </table>
    </div><!-- /.box-body -->

    </div><!-- /.box -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->