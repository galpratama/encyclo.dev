<?php $products = $products_pos->row();?>
<?php $products_details = $products_webstore->row();?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Product Details "<?php echo $products->nmproduk  ?>"
            <small>Catalog</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-archive"></i> Catalog</a></li>
            <li><a href="#"> Product</a></li>
            <li class="active">Edit Details </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-xs-12">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_content" data-toggle="tab">Product Details</a></li>
                    <li class="pull-right header">
                        <form enctype="multipart/form-data" role="form" action="<?php echo base_url();?>nycadmin/products/update_details" method="post">
                            <button type="submit" class="btn btn-success btn-md"><i class="fa fa-save"></i> Save Changes</button>
                        <button type="reset" class="btn btn-warning btn-md"><i class="fa fa-refresh"></i> Reset</button>
                        <button type="button" onclick="goBack()" class="btn btn-danger btn-md"><i class="fa fa-trash-o"></i> Discard</button>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_content">
                        <div class="form-group">
                            <label for="">Product Name</label>
                            <input type="text" name="nmproduk_readonly" class="form-control" placeholder="" value="<?php echo $products->nmproduk  ?>" disabled readonly>
                        </div>
                        <div class="form-group">
                            <label for="">SKU</label>
                            <input type="text" name="kdproduk" class="form-control" placeholder="" value="<?php echo $products->kdproduk  ?>" disabled readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Sale Price</label>
                            <input type="text" name="sale_price" class="form-control" placeholder="If theres any sale price, type here" value="<?php echo $products_details->sale_price ?>">
                        </div>
                        <div class="form-group">
                            <label for="">SEO Title</label>
                            <input type="text" name="seo_title" class="form-control" placeholder="Example: The Best Shafira Encyclo Produts" value="<?php echo $products_details->seo_title ?>">
                        </div>
                        <div class="form-group">
                            <label for="">SEO Meta</label>
                            <textarea name="seo_meta" class="form-control"><?php echo $products_details->seo_meta ?></textarea>
                        </div>
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            <input type="hidden" name="r_produk_id" value="<?php echo $products->r_produk_id?>"/>
            <input type="hidden" name="nmproduk" value="<?php echo $products->nmproduk ?>"/>
            </form>
        </div><!-- nav-tabs-custom -->
    </div>
    </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->
